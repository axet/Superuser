#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

"$DIR/install.sh"

adb "$@" shell setenforce 0
adb "$@" shell su --daemon