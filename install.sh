#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

CPU=$(adb "$@" shell getprop ro.product.cpu.abi)
SOURCE="Superuser/libs/${CPU//[[:space:]]/}/su"

[ -e "$SOURCE" ] || ( cd Superuser && ndk-build )

adb "$@" root
adb "$@" remount
adb "$@" push "$SOURCE" /system/xbin/su
adb "$@" shell chown -v root:root /system/xbin/su
adb "$@" shell chmod -v 6755 /system/xbin/su
